
# Cycle Waster

This is a simple application that uses the 'mandle' program
in an infinite loop to keep a system busy with some cpu load.


## Running

This application is all ready to be used with Docker/Docker-Compose.

To set it up simply run the following commands:

    $ git clone https://gitlab.com/Fezent/cycle-waster.git
    $ cd cycle-waster
    $ docker compose up -d

Now you should have a nice little container happily wasting your
CPU cycles. To shut it down just run:

    $ docker compose down