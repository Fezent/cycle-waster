
# Build Mandle
FROM alpine:latest AS build
RUN apk add --update git build-base
RUN git clone https://gitlab.com/henrystanley/mandle.git
WORKDIR mandle
RUN make

# Run Cycle-Waster
FROM alpine:latest as run
WORKDIR /cycle-waster
COPY --from=build /mandle/mandle /cycle-waster
ADD cycle-waster.sh .
ENTRYPOINT [ "sh", "./cycle-waster.sh"]
